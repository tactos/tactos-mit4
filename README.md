# TACTOS-MIT4

![tactos-MIT4 interface](doc/img/screenshot_application.png)


Tactos-MIT4 is the driver to connect tactos-core with a MIT4 module. MIT4 is a braille device with only 2 cells connected to the computer by bluetooth. This module has been developped by the [University of Technology of Compiegne (UTC)](https://www.utc.fr/)

This  software is designed to work with [tactos-core-color](https://gitlab.com/tactos/tactos-core-color)  software. 

## Building

### Dependecies

- meson (build system)
- gtkmm-3.0 
- yaml-cpp 
- bluetooth 
- giomm (dbus dependency)


## Compilation

 Tactos-MIT4 uses [meson](https://mesonbuild.com/) as build system.

```bash
meson --buildtype=release builddir -Dprefix=$(pwd)/builddir
cd builddir
ninja install
cd Tactos 
./Tactos-MIT4
```

### Documentation

A doxygen documentation can be found [here](https://tactos.gitlab.io/tactos-mit4/)

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/). First version has been developped by Clément Ferry.
