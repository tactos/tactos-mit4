#pragma once

#include <thread>

#include "workeripc.h"
#include "workersender.h"

#include<filesystem>
#include "device.h"

namespace Tactos
{
  class DeviceWindow;

  /**
   * Manage the bluetooth connection
   * and create threads when a connection is established
   */
  class GuiConnection : public WorkerCaller
  {
  public:
    GuiConnection(DeviceWindow* window, std::filesystem::path configfile);
    void on_notification_from_worker_thread();

    void set_device(std::string addr, unsigned int chan);
    const BluetoothDevice& get_device() ;

    bool is_connected() {return m_device != nullptr;}
    void disconnect_device();
    void update_dbus_status(Glib::RefPtr<Gtk::Label> dbus);
    Matrix& matrix();


  private:

    DeviceWindow* const m_window;
    // Connected device (null if disconnected)
    std::unique_ptr<BluetoothDevice> m_device;
    std::unique_ptr<std::thread> m_WorkerIPCThread;
    std::unique_ptr<std::thread> m_WorkerSenderThread;

    WorkerIPC m_WorkerIPC;
    WorkerSender m_WorkerSender;

    Matrix m_matrix;
  };
}
