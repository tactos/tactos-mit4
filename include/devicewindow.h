#pragma once

// Widgets
#include <gtkmm/dialog.h>
#include <gtkmm/builder.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/spinbutton.h>

//Threads
#include <glibmm/dispatcher.h>
#include <thread>

#include "guiconnection.h"

#include "workerscan.h"
#include "workerconnection.h"

#include <string>
#include <memory>
#include <iostream>
#include <filesystem>

#include "device.h"


namespace Tactos
{
  /**
   * This class represents the configuration window
   */
  class DeviceWindow : public Gtk::Dialog, public WorkerCaller
  {
  public:
    DeviceWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade, std::filesystem::path file);

    //slots
    void on_button_scan_clicked();
    void on_button_connect_clicked();
    void on_select_device();

    void update_connect_button();

    // Update list of device
    void add_device_list(std::string name, std::string mac_addr);
    void reset_device_list() {m_listDevices->clear();};

    // Setters
    void set_device(std::string addr, unsigned int chan);

    // Allow workers to emit signal
    void on_notification_from_worker_thread();

    // Selected device
    std::string getSelectedDeviceMAC() const;
    std::string getSelectedDeviceName() const;
    unsigned int getFrequency() const {return m_frequency->get_value_as_int();}

    // Factorisation of workers' launchers
    void launch_worker(Worker<DeviceWindow>* worker);
    Glib::RefPtr<Gtk::Label> m_status;
    Glib::RefPtr<Gtk::Label> m_DBus;

  private:
    //Widgets in the window
    Glib::RefPtr<Gtk::Builder> m_ui;
    Glib::RefPtr<Gtk::Button> m_scan;
    Glib::RefPtr<Gtk::Button> m_connect;
    Glib::RefPtr<Gtk::TreeView> m_treeview;
    Glib::RefPtr<Gtk::ListStore> m_listDevices;
    Glib::RefPtr<Gtk::SpinButton> m_frequency;
    Glib::RefPtr<Gtk::Adjustment> m_adjustmentFrequency;

    // Model of list of devices
    class ColumnsDevices : public Gtk::TreeModel::ColumnRecord
    {
    public:
      Gtk::TreeModelColumn<Glib::ustring> name;
      Gtk::TreeModelColumn<Glib::ustring> mac_addr;
      ColumnsDevices() { add(name); add(mac_addr); }
    };
    ColumnsDevices m_columns;

    //Scan thread
    Glib::Dispatcher m_Dispatcher;
    WorkerScan m_WorkerScan;
    WorkerConnection m_WorkerConnect;
    Worker<DeviceWindow>* m_currentWorker;
    std::unique_ptr<std::thread> m_WorkerThread; // Scan and connect

    GuiConnection m_bluetoothConnection;

    std::string m_selectedDeviceName;
    std::string m_selectedDeviceMAC;

  };
}
