#pragma once

#include "worker.h"

namespace Tactos
{
  class DeviceWindow;

  /**
   * @brief This worker scan the bluetooth network. he can't be stopped
   */
  class WorkerScan : public Worker<DeviceWindow>
  {
  public:
    WorkerScan() : Worker<DeviceWindow>() {}
    void do_work(DeviceWindow* caller);
  };
}
