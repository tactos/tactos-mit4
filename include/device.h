#pragma once
#include<string>
#include<vector>
#include<exception>

#include <yaml-cpp/yaml.h>

namespace Tactos
{
  #define DEVICE_BYTE_1 0x1b 
  #define DEVICE_BYTE_2 0x01 
  #define CHANNEL 6 

  typedef std::vector<std::pair<std::string, std::string> > List_Dev;

  /**
   * Exception threw when connection failed
   * @param info Raisons why the exception is throw
   */
  class BluetoothConnectException : public std::exception
  {
  public:
    BluetoothConnectException(std::string info = "") : m_info(info) {}
    const char* what() const throw() { return m_info.c_str(); }
  private:
    std::string m_info;
  };

  
  /**
   * This class represents a Device. It is an abstract class.
   * Childs should contain connection method (USB, Bluetooth, etc...)
   */
  class Device
  {
  public:
    virtual void send(char* bytes) const = 0;
    virtual ~Device() = default;
  };

  
  /**
   * Specialisation of Device class for MIT4 module. This module is managed by a bluetooth connection.
   * @param mac_addr MAC address of the device
   * @param channel  channel connection
   */
  class BluetoothDevice : public Device
  {
  public:
    static const char CONTROL_BYTE_1 = 0x1b; ///< first control byte for MIT4 device
    static const char CONTROL_BYTE_2 = 0x01; ///< second control byte for MIT4 device
    static const uint8_t SIZE_TRAME = 2; ///< number of byte to send. sould be the number of braille cells
    static const uint8_t BLUETOOTH_CHANNEL = 6; ///< bluetooth channel of MIT4
      
    BluetoothDevice(std::string mac_addr, uint8_t channel);
    virtual ~BluetoothDevice();
    static List_Dev scan();

    void send(char* bytes) const override;

  private:
    int m_socket;
  };
}
