#pragma once

#include "worker.h"

namespace Tactos
{
  class DeviceWindow;

  /**
   * @brief This worker manage connection. When launched it can't be stopped.
   */
  class WorkerConnection : public Worker<DeviceWindow>
  {
  public:
    WorkerConnection() : Worker<DeviceWindow>() {}
    void do_work(DeviceWindow* caller);
  };
}
