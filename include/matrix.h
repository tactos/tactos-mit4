#pragma once

#include <vector>
#include <utility> //pair
#include <string>
#include <filesystem>

#include "device.h" //SIZE_TRAME

namespace Tactos
{
  /**
   *This class represents a braille matrix of a device.
   */
  class Matrix
  {
  public:
    Matrix( std::filesystem::path pathYamlFile, unsigned int size = BluetoothDevice::SIZE_TRAME*8);
    void put(std::vector<bool> state);
    void get(char* bytes) const;
    const std::vector<bool>& get_state() const {return m_state;}
    void reset();
    static std::vector<std::pair<int, int> > LoadMapFile(unsigned int size, std::string path);
    int nbCells() const;
    bool sent = false; ///< True if data have already been send 

  private:
    std::vector<bool> m_state; ///< next values to send to device
    std::vector<std::pair<int, int> > m_map; ///< mapping between m_state grid and device pins grid
    uint8_t m_size; ///< number of pins
  };
}
std::ostream &operator<<(std::ostream &os, Tactos::Matrix const &m);
