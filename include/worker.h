#pragma once

#include <mutex>
#include <glibmm/dispatcher.h>

namespace Tactos
{
  class WorkerCaller
  {
  public:
    WorkerCaller() {m_dispatcher.connect(sigc::mem_fun(*this, &WorkerCaller::on_notification_from_worker_thread));};
    virtual ~WorkerCaller() = default;

    void notify() {m_dispatcher.emit();}
    virtual void on_notification_from_worker_thread() = 0;

  protected:
    Glib::Dispatcher m_dispatcher;
  };

  /**
   * Abstract class for DeviceWindow's workers
   */
  template<class T_Caller>
  class Worker
  {
  public:
    /**
     * Constructor of Worker. Worker is subroutine of GUI.
     */
    Worker() : m_has_stopped(true) {}

    // Thread function.
    virtual void do_work(T_Caller* caller) = 0;

    /**
     * State of the worker
     * @return State of the worker (true if the worker is stopped)
     */
    bool has_stopped() const {return m_has_stopped;}

    /**
     * Stop the worker
     */
    void stop() {m_has_stopped = true;}

    virtual ~Worker() = default;

  protected:
    // Data used by both GUI thread and worker thread.
    bool m_has_stopped;
  };
}
