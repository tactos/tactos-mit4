#pragma once

#include <thread>

#include "worker.h"
#include "matrix.h"


namespace Tactos
{
  class GuiConnection;
  class DeviceWindow;

  /**
   * @brief This class represents the worker witch read the shared memory
   */
  class WorkerSender : public Worker<GuiConnection>
  {
  public:
    WorkerSender(DeviceWindow* window)
     : Worker<GuiConnection>(), m_window(window) {}

    void do_work(GuiConnection* caller);

  private:
      DeviceWindow* m_window;
  };
}
