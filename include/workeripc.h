#pragma once

#include <thread>

#include "worker.h"
#include "tactosIPC_proxy.h"
#include "gtkmm.h"

#define WELL_KNOWN_NAME "org.tactos.ipc"
#define WELL_KNOWN_OBJECT "/org/tactos/ipc"

namespace Tactos
{
  class GuiConnection;

  /**
   * @brief This class represents the worker witch listen dbus interface
   */
  class WorkerIPC : public Worker<GuiConnection>
  {
  public:
      WorkerIPC();
      ~WorkerIPC();
      void do_work(GuiConnection* caller);
      void on_update_publisher();
      void setDbusLabel(Glib::RefPtr<Gtk::Label> m_DBus);
      void stop();
  private:
      void on_update_matrix();
      Glib::RefPtr<Gtk::Label> m_dbus_status;
      GuiConnection* m_caller;
      Glib::RefPtr<org::tactos::ipcProxy> m_proxy;
      sigc::connection m_matrix_connection;
  };
}
