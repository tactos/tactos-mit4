#include <iostream>

#include "workeripc.h"

#include "device.h"
#include "workersender.h"


#include<vector>
#include "matrix.h"
#include <guiconnection.h>
#include <devicewindow.h>

using namespace Tactos;

/**
 * @brief callback function called when dbus matrix is updated. Update local copy of matrix.
 * 
 * @param proxy Dbus Tactos proxy
 * @param mat ref to matrix to update
 */
void WorkerIPC::on_update_matrix()
{
    bool valid = true;
    std::vector<bool> value = this->m_proxy->Matrix_get(&valid);
    if(valid) {
        this->m_caller->matrix().put(value);
    }
}
void Tactos::WorkerIPC::on_update_publisher()
{
    if (this->m_proxy->Publisher_get() == false) {
        this->m_dbus_status->set_text("Tactos non lancé !");
    } else{
        this->m_dbus_status->set_text("Tactos lancé !");        
    }
}



Tactos::WorkerIPC::WorkerIPC(): Worker<GuiConnection>()
{
    Glib::init();
    Gio::init();
    this->m_proxy = org::tactos::ipcProxy::createForBus_sync(
        Gio::DBus::BUS_TYPE_SESSION,
        Gio::DBus::PROXY_FLAGS_NONE,
        WELL_KNOWN_NAME,
        WELL_KNOWN_OBJECT,
        Gio::Cancellable::create()
    );
    this->m_proxy->Publisher_changed().connect(sigc::mem_fun(this, &WorkerIPC::on_update_publisher));
}

void Tactos::WorkerIPC::setDbusLabel(Glib::RefPtr<Gtk::Label> m_DBus)
{
    this->m_dbus_status = m_DBus;
    this->on_update_publisher();
}

/**
* connect to dbus Matrix state and update local copy of it.
* @param caller Window witch call the worker
*/
void WorkerIPC::do_work(GuiConnection* caller)
{
    this->m_caller = caller;
    this->m_matrix_connection = this->m_proxy->Matrix_changed().connect(sigc::mem_fun(this, &WorkerIPC::on_update_matrix));
    this->m_proxy->readerJoin_sync();
}

/**
 * @brief unsuscribe to tactos reader and matrix update.
 * 
 */
void WorkerIPC::stop()
{
    if (this->m_matrix_connection.connected())
    {
        this->m_proxy->readerLeave_sync();
        this->m_matrix_connection.disconnect();
    }
}

/**
 * @brief quit DBus mainloop
 * 
 */
Tactos::WorkerIPC::~WorkerIPC()
{
    this->stop();    
}
