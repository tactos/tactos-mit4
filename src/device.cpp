#include "device.h"

#include <iostream>

#include <stdexcept>
#include <string>

#include <unistd.h> //write

#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

//scan
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
//channel
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>

using namespace Tactos;

/**
 * @brief Constructor of BluetoothDevice
 * 
 * @param mac_addr MAC address of the device
 * @param channel  COnnection channel
 */
BluetoothDevice::BluetoothDevice(std::string mac_addr, uint8_t channel)
{
  int bluetoothControllerId = hci_get_route(NULL);
  if(bluetoothControllerId < 0) throw BluetoothConnectException("No Bluetooth Adapter Available");

  struct sockaddr_rc addr = {0, {BDADDR_BREDR}, channel};
  //Allocate a socket
  m_socket = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

  //Set the connetion paramenters
  addr.rc_family = AF_BLUETOOTH;
  str2ba(mac_addr.c_str(), &addr.rc_bdaddr);

  //Connect to server
  int status = connect(m_socket, (struct sockaddr *) &addr, sizeof(addr));

  if(status == -1) throw BluetoothConnectException("failed to connect (status = -1)");
}

/**
  * @brief Destructor of BluetoothDevice
  * Close the bluetooth connection
  */
BluetoothDevice::~BluetoothDevice() { close(m_socket); }

/**
  * Send SIZE_TRAME bytes to the device
  * @param bytes Bytes to send
  */
void BluetoothDevice::send(char bytes[SIZE_TRAME]) const
{
    
    char trame[SIZE_TRAME+2] = {CONTROL_BYTE_1, CONTROL_BYTE_2, bytes[0], bytes[1]};
    write(m_socket, trame, SIZE_TRAME+2);
}

/**
  * Make the list of available networks
  * %return List of pair of (name of device, MAC address of device)
  */
Tactos::List_Dev BluetoothDevice::scan()
{
  Tactos::List_Dev foundDevices;

  char addr[19] = {0};
  char name[248] = {0};

  int device_id = hci_get_route(NULL);
  int socket = hci_open_dev( device_id );
  if (device_id < 0 || socket < 0)
    throw BluetoothConnectException("failed to open socket (scan)");

  int len  = 8;
  int max_devices = 255;

  inquiry_info* ii = nullptr;
  try // Allocation of inquiry_info
  {
    ii = new inquiry_info[max_devices];
    int num_rsp = hci_inquiry(device_id, len, max_devices, NULL, &ii, IREQ_CACHE_FLUSH);
    if( num_rsp < 0 ) throw BluetoothConnectException("hci_inquiry");

    // For each found device
    for (int i = 0; i < num_rsp; i++) {
        // Get MAC address
        ba2str(&ii[i].bdaddr, addr);

        memset(name, 0, sizeof(name));
        if (hci_read_remote_name(socket, &ii[i].bdaddr, sizeof(name), name, 0) < 0)
          strcpy(name, "[unknown]");

        foundDevices.push_back(std::pair<std::string, std::string>(std::string(name), std::string(addr)));
    }

    delete[] ii;
  } catch (std::exception& e)
  {
    delete[] ii;
    throw e;
  }
  close(socket);


  return foundDevices;
}
