#include <iostream>
#include "shared_mat.h"

// using namespace std;

int main(int argc, char* argv[])
{
	unsigned int size = 16;
	Tactos::SharedMatWriter ipc("tactos", size);
	bool reset = true;
	std::vector<bool> state(size, false);

	while(true)
	{
		for(unsigned int i = 0; i < size; i++)
		{
			//cout << "STATE: Modification de la variable d'état (i=" << i << ")" << endl;
			if(i>0) state[i-1] = false;
			else state[size] = false;
			state[i] = true;

			//cout << "IPC:\tEnvoi au segment de mémoire partagé" << endl;
			ipc.write(state);

			//usleep(1 * 1000);

			std::cout << std::endl;
		}
		reset = !reset;
	}

	return 0;
}
