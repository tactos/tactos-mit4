#include <iostream>
#include <gtkmm/treeselection.h>

#include "devicewindow.h"
#include "device.h"

using namespace Tactos;
  /**
   * Constructor of DeviceWindow
   * @param cobject  Specific object of gtkmm
   * @param refGlade Builder of the window (should be define with glade file)
   */
  DeviceWindow::DeviceWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade, std::filesystem::path file)
    : Gtk::Dialog(cobject),
      m_ui(refGlade),

      m_adjustmentFrequency(Gtk::Adjustment::create(50.0, 1.0, 10000.0, 10.0)),
      m_Dispatcher(),
      m_WorkerScan(),
      m_WorkerConnect(),
      m_currentWorker(nullptr),
      m_WorkerThread(nullptr),
      m_bluetoothConnection(this, file)
  {
    resize(300, 200);

    if(m_ui)
    {
      // Get all useful widget on glade file
      m_status = Glib::RefPtr<Gtk::Label>::cast_dynamic(
        m_ui->get_object("connectionStatus")
      );

      // Get all useful widget on glade file
      m_DBus = Glib::RefPtr<Gtk::Label>::cast_dynamic(
          m_ui->get_object("dbusStatus")
      );
      m_scan = Glib::RefPtr<Gtk::Button>::cast_dynamic(
        m_ui->get_object("buttonScan")
      );

      m_connect = Glib::RefPtr<Gtk::Button>::cast_dynamic(
        m_ui->get_object("buttonConnect")
      );
      m_frequency = Glib::RefPtr<Gtk::SpinButton>::cast_dynamic(
        m_ui->get_object("frequency")
      );

      m_frequency->set_adjustment(m_adjustmentFrequency);


      // Widget for list of device
      m_treeview = Glib::RefPtr<Gtk::TreeView>::cast_dynamic(
        m_ui->get_object("treeview")
      );
      m_listDevices = Gtk::ListStore::create(m_columns);
      m_treeview->set_model(m_listDevices);
      m_treeview->get_selection()->set_mode(Gtk::SELECTION_BROWSE);
      m_treeview->set_activate_on_single_click(true);

      // Create columns
      m_treeview->append_column("Nom", m_columns.name);
      m_treeview->append_column("Adresse MAC", m_columns.mac_addr);

      // Signals
      m_scan->signal_clicked().connect( sigc::mem_fun(*this, &DeviceWindow::on_button_scan_clicked) );
      m_connect->signal_clicked().connect( sigc::mem_fun(*this, &DeviceWindow::on_button_connect_clicked) );
      m_treeview->get_selection()->signal_changed().connect( sigc::mem_fun(*this, &DeviceWindow::on_select_device) );

      // Update to initial state
      update_connect_button();

      this->m_bluetoothConnection.update_dbus_status(this->m_DBus);
      show_all();
    }
  }

  /**
   * @brief Run a worker if there is no worker launched
   * 
   * @details A worker is a subroutine launched to perform task. There is currently 5 types of worker.
   * @param worker worker to run
   */
  void DeviceWindow::launch_worker(Worker<DeviceWindow>* worker)
  {
    // Only one worker should be run
    if (m_WorkerThread)
      std::cout << "Can't start a worker thread while another one is running." << std::endl;
    else
    {
      // Start a new worker thread.
      m_currentWorker = worker;
      m_WorkerThread = std::unique_ptr<std::thread>(new std::thread(
        [this]
        {
          m_currentWorker->do_work(this);
        }));
    }
  }

  /**
   * Slot for the button scan
   */
  void DeviceWindow::on_button_scan_clicked()
  {
    // Try to run scan worker
    launch_worker(&m_WorkerScan);
  }

  /**
   * Slot for the button connection/deconnection
   */
  void DeviceWindow::on_button_connect_clicked()
  {
    if(m_bluetoothConnection.is_connected() && m_WorkerConnect.has_stopped())
      /* Deconnection: the worker call the destructor
       * of BluetoothDevice to disconnect the device
       */
      m_bluetoothConnection.disconnect_device();
    else
      // Connection
      launch_worker(&m_WorkerConnect);
  }

  /**
   * Slot called when a device is selectionned
   */
  void DeviceWindow::on_select_device()
  {
    Gtk::TreeModel::iterator iter = m_treeview->get_selection()->get_selected();
    if(iter) //If anything is selected
    {
      Gtk::TreeModel::Row row = *iter;
      m_selectedDeviceName = row.get_value(m_columns.name);
      m_selectedDeviceMAC = row.get_value(m_columns.mac_addr);
    }
  }

  /**
   * Update the connect button according to
   * the state of the connection (no thread safety)
   */
  void DeviceWindow::update_connect_button()
  {
    if(m_bluetoothConnection.is_connected())
      m_connect->set_label("Déconnecter");
    else
      m_connect->set_label("Connecter");
  }

  /**
   * Slot called when a worker notify the window. That should appear when the task of the worker is done
   */
  void DeviceWindow::on_notification_from_worker_thread()
  {
    // When a worker notify the window
    if (m_WorkerThread && m_currentWorker->has_stopped())
    {
      // Work is done.
      if (m_WorkerThread->joinable())
        m_WorkerThread->join();

      m_WorkerThread = nullptr;
      m_currentWorker = nullptr;
    }

    update_connect_button();
  }

  /**
   * Connect to the considered device
   * @param addr MAC address of the device
   * @param chan connection channel
   */
  void DeviceWindow::set_device(std::string addr, unsigned int chan)
  {
    m_bluetoothConnection.set_device(addr, chan);
  }

  /**
   * Add a device on the list displayed
   * @param name     Name of the device
   * @param mac_addr Address MAC of the device
   */
  void DeviceWindow::add_device_list(std::string name, std::string mac_addr)
  {
    Gtk::TreeModel::Row row = *(m_listDevices->append());
    row[m_columns.name] = name;
    row[m_columns.mac_addr] = mac_addr;
  }

  /**
   * Get the MAC address of the selected device
   * @return MAC address of the selected device
   */
  std::string DeviceWindow::getSelectedDeviceMAC() const
  {
    return m_selectedDeviceMAC;
  }

  /**
   * Get the name of the selected device
   * @return Name of the selected device
   */
  std::string DeviceWindow::getSelectedDeviceName() const
  {
    if(m_selectedDeviceName == "")
      return "[None]";
    else
      return m_selectedDeviceName;
  }
