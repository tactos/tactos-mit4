#include "matrix.h"
#include <glibmm/miscutils.h>

using namespace Tactos;
  /**
   * @brief Constructor of Matrix. The YAML file is used to do correspondance between matrix values and pins.
   * @param size         Number of pin
   * @param pathYamlFile Path of YAML file
   */
  Matrix::Matrix(std::filesystem::path pathYamlFile, unsigned int size):
  m_size(size)
  {
      
    m_state = std::vector<bool>(size, 0);
    m_map = LoadMapFile(size, pathYamlFile);
  }
  
  /**
   * @brief Get up some pin without get down pins if pins didn't be sent to device.
   * 
   * @details pins are hardware material so that in has a limited frequency that can be slower than the 
   * detection on the screen. To be sure to keep all detected information, we refresh often the matrix 
   * representation with an OR operation and reset it only when we write it on the device.
   * @param state New wanted state
   */
  void Matrix::put(std::vector<bool> state)
  {
      
      unsigned int size = state.size();
      if(size != m_state.size()) throw std::invalid_argument("state size"); 
      
      if (this->sent)
      {
        for(unsigned int i = 0; i < size; ++i)
        {
          this->m_state[i] = (state[i] == 1);
        }
      } else
      {
        for(unsigned int i = 0; i < size; ++i)
        {
          m_state[i] = m_state[i] || (state[i] == 1);
        }
          
      }
      this->sent = false;
  }

  /**
   * @brief Get bytes in order to send matrix to the device
   * 
   * @details due to historical reason, braille pins are not saved has normal matrix index.
   * We need to perform a traduction operation between matrix value and pin positions
   * @param bytes Bytes according the device order
   */
  void Matrix::get(char* bytes) const
  {
  for(unsigned int i = 0; i < this->m_size; ++i) bytes[i] = 0;

    unsigned int size = m_state.size();
    for(unsigned int i = 0; i < size; ++i)
      if(m_state.at(i)) //For each considered states
      {
        std::pair<int, int> index = m_map[i];
        bytes[index.first] += 1 << index.second;
      }
  }

  /**
   * @brief Reset the state of the matrix . 
   * 
   * @details set the representation matrix to all 0. Used after writing to the device.
   */
  void Matrix::reset()
  {
    for(std::vector<bool>::iterator it = m_state.begin(); it != m_state.end(); ++it)
      *it = false;
  }

  /**
   * @brief Load a YAML file
   * 
   * @details load traduction file betwen the matrix index and the device index. format of the YAML file is : 
   * 
   * ~~~~~~~~~~~~~{.yml}
   * matrix Index: [braille cell, pin number] 
   * ~~~~~~~~~~~~~~~
   * @param size Number of considred pin
   * @param path YAML file path
   * @return List of pair of (module, index)
   */
  std::vector<std::pair<int, int> > Matrix::LoadMapFile(unsigned int size, std::string path) {
    std::vector<std::pair<int, int> > map;
    try {
      YAML::Node yamlContent = YAML::LoadFile(path);     
    
      //For each coordinate
      for(unsigned int i = 1; i <= size; i++)
      {
        std::vector<int> index = yamlContent[i].as<std::vector<int>>();
        if(index.size() != 2)
          throw std::length_error("Syntax error: " + path);

        map.push_back(std::pair<int, int>(index[0], index[1]));
      }
      
    } catch (std::exception &e)
    {
      throw std::runtime_error("ERROR : error while reading Yaml file on : " + path);
    }

    return map;
  }

  /**
   * @brief get number of cells in the device
   * 
   * @return int
   */
  int Tactos::Matrix::nbCells() const
  {
      return this->m_size;
  }
  
  
  /**
   * @brief display nicely matrix class
   * 
   * @param os ostream
   * @param m matrix to display
   * @return formatted output
   */
  std::ostream &operator<<(std::ostream &os, Matrix const &m) 
  { 
      const int length = m.nbCells()*2;
      auto mat = m.get_state();
      os << " " << std::string(length, '_') << " " << std::endl << "|";
      std::copy(mat.begin(), mat.begin()+length, std::ostream_iterator<int>(os, "")); 
      os << "|" << std::endl << "|";
      std::copy(mat.begin()+length, mat.begin()+2*length, std::ostream_iterator<int>(os, "")); 
      os << "|" << std::endl << "|";
      std::copy(mat.begin()+2*length, mat.begin()+3*length, std::ostream_iterator<int>(os, "")); 
      os << "|" << std::endl << "|";
      std::copy(mat.begin()+3*length, mat.end(), std::ostream_iterator<int>(os, "")); 
      os << "|" << std::endl << " " << std::string(length, '_') << " " << std::endl;
      return os;
  }

