#include "guiconnection.h"

#include "devicewindow.h"

using namespace Tactos;
  /**
   * @brief Constructor of GuiConnection
   * @param window Window witch make connection
   */
  GuiConnection::GuiConnection(DeviceWindow* window, std::filesystem::path configfile)
   : m_window(window),
   m_device(nullptr),
   m_WorkerIPCThread(nullptr),
   m_WorkerSenderThread(nullptr),
   m_WorkerIPC(),
   m_WorkerSender(window),
   m_matrix(configfile)
  {}

  /**
   * @brief Kill threads when their are stopped
   */
  void GuiConnection::on_notification_from_worker_thread()
  {
    if(m_WorkerSenderThread && m_WorkerSender.has_stopped())
    {
      if(m_WorkerSenderThread->joinable())
        m_WorkerSenderThread->join();

      m_WorkerSenderThread = nullptr;
      m_device.reset(nullptr);
    }

    m_window->update_connect_button();
  }

  /**
   * @brief Connect a device
   * @param addr MAC address of the device
   * @param chan Channel of connection
   */
  void GuiConnection::set_device(std::string addr, unsigned int chan)
  {
    m_device.reset(new BluetoothDevice(addr, chan));

    if(m_WorkerIPCThread != nullptr)
    {
      m_WorkerIPC.stop();
    }
    
    m_WorkerIPCThread.reset(new std::thread(
    [this]
    {
        m_WorkerIPC.do_work(this);
    }));
    if(m_WorkerSenderThread != nullptr)
    {
        m_WorkerSender.stop();
    }
    m_WorkerSenderThread.reset(new std::thread(
    [this]
    {
        m_WorkerSender.do_work(this);
    }));
  }
 
 /**
  * @brief get publisher value from Dbus to update label about tactos publisher activation
  * 
  * @param dbus label to update
  */
 void Tactos::GuiConnection::update_dbus_status(Glib::RefPtr<Gtk::Label> dbus)
 { 
     this->m_WorkerIPC.setDbusLabel(dbus);
}

  /**
   * @brief Disconnect the device
   */
  void GuiConnection::disconnect_device()
  {
    m_WorkerIPC.stop();
    m_WorkerSender.stop();
    m_device.reset(nullptr);
    this->m_window->m_DBus->set_text("Déconnecté");
  }
  
  /**
   * @brief return ref to connected device. nullptr if no device connected.
   * 
   * @return const Tactos::BluetoothDevice& connected device.
   */
  const BluetoothDevice& GuiConnection::get_device()
  {
      return *m_device;
      
  }
  
  /**
   * @brief return the matrix containing values of the device.
   * 
   * @return Matrix&
   */
  Matrix& GuiConnection::matrix()
   {
       return m_matrix;
   }

