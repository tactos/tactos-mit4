#include "workersender.h"
#include "guiconnection.h"
#include "devicewindow.h"

using namespace Tactos;
  /**
   * Write the representation matrix to the device and reset it.
   * @param caller Window witch call the worker
   */
  void WorkerSender::do_work(GuiConnection* caller)
  {
    m_has_stopped = false;

    char bytes[] = {0, 0}; // FIXME work only for a 2cells matrix
    while(!m_has_stopped)
    {
      {
        caller->matrix().get(bytes);
        caller->get_device().send(bytes);
        caller->matrix().sent = true;
      }
       usleep(1000000/m_window->getFrequency());
    }

    // End of thread
    caller->notify();
  }
