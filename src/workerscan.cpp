#include <iostream>
#include <thread>
#include <mutex>

#include "workerscan.h"

#include "device.h"
#include "devicewindow.h"

using namespace Tactos;
  /**
   * Call the bluetooth scan function and update the widget
   * @param caller Window witch call the worker
   */
  void WorkerScan::do_work(DeviceWindow* caller)
  {
    caller->reset_device_list();
    m_has_stopped = false;

    caller->m_status->set_text("Scan du réseau");

    // The reason why we need a thread (long duration)
    List_Dev devices = BluetoothDevice::scan();
    {
      // Add each device in the widget
      for (auto device : devices)
        caller->add_device_list(device.first, device.second);

      caller->m_status->set_text("Scan effectué");
    }

    // End of thread
    m_has_stopped = true;
    caller->notify();
  }
