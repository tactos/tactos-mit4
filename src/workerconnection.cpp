#include <iostream>
#include <thread>
#include <mutex>

#include "workerscan.h"

#include "device.h"
#include "devicewindow.h"

using namespace Tactos;
  /**
   * Make a connection with the selected device
   * @param caller Window witch call the worker
   */
  void WorkerConnection::do_work(DeviceWindow* caller)
  {
    m_has_stopped = false;
    std::string name, mac_addr;

    name = caller->getSelectedDeviceName();
    mac_addr = caller->getSelectedDeviceMAC();

    if(mac_addr.size() > 12)
      name.append(" (" + mac_addr.substr(12) + ")");
    caller->m_status->set_text("Connexion à " + name + " en cours");

    // If there are no device selectioned
    if(mac_addr == "")
    {
      caller->m_status->set_text("Aucun périphérique sélectionné");
      m_has_stopped = true;
      caller->notify();
      return;
    }

    try {
      caller->set_device(mac_addr, CHANNEL);
      caller->m_status->set_text("Connecté à " + name);
    } catch(BluetoothConnectException& e)
    {
      caller->m_status->set_text("Connexion Échouée");
    }

    // End of thread
    m_has_stopped = true;
    caller->notify();
  }
