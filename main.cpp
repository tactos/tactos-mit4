#include <iostream>
#include <string>
#include <vector>
#include <filesystem>
#include<stdexcept>

typedef std::filesystem::path path;

#include <unistd.h>
#include "device.h"
#include "matrix.h"
#include <glibmm/miscutils.h>

#include "devicewindow.h"

#define DEFAULT_YAML_FILE "MIT4.yml"

path getFilePath(std::string filename = DEFAULT_YAML_FILE)
{
    path file;
    std::string dataDirs = std::getenv("XDG_DATA_DIRS");
    if (dataDirs.empty())
    {
        dataDirs = "/usr/local/share/:/usr/share/";
    }
    
    std::vector<path> default_paths;
    default_paths.push_back(path());
    size_t pos = 0;
    while ((pos = dataDirs.find(':')) != std::string::npos) {
        default_paths.push_back(path(dataDirs.substr(0, pos)) /= "tactos");
        dataDirs.erase(0, pos + 1);
    }
    
    for (auto path : default_paths)
    {
        file = path /= filename;
        if (std::filesystem::is_regular_file(file))
        {
            return file;
        }
    }
    throw std::invalid_argument(filename + " not found"); 
}
    
int main(int argc, char* argv[])
{
    path file;
    if(argc >= 2)
    {
        file = getFilePath(argv[1]);
    } else
    {
        file = getFilePath();        
    }
  auto app = Gtk::Application::create("org.tactos.device");

  //Load the Glade file and instantiate its widgets:
  auto refBuilder = Gtk::Builder::create_from_resource("/interface/window.glade");
  
  //Get the GtkBuilder-instantiated dialog:
  Tactos::DeviceWindow* pDialog = nullptr;
  try
  {
    refBuilder->get_widget_derived("window", pDialog, file);
    if(pDialog) app->run(*pDialog);
    delete pDialog;
  } catch(std::exception& e)
  {
    delete pDialog;
    std::cout <<"main exception : " << e.what() << std::endl;
    throw e;
  }

  return 0;
}
